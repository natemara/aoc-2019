use aoc_runner_derive::aoc_lib;

mod directions;
mod intcode;
mod iter;
mod util;

#[cfg(test)]
#[macro_use]
mod testutil;

mod day1;
mod day10;
mod day11;
mod day12;
mod day13;
mod day2;
mod day3;
mod day4;
mod day5;
mod day6;
mod day7;
mod day8;
mod day9;

aoc_lib! { year = 2019 }
