use crate::directions::*;
use crate::util::parse_lines_and_commas;
use aoc_runner_derive::{aoc, aoc_generator};
use std::collections::HashMap;
use std::collections::HashSet;
use std::i32;

#[aoc_generator(day3)]
pub fn gather_input(input: &str) -> Vec<Vec<DirectionAndMagnitude>> {
    parse_lines_and_commas(input)
}

#[aoc(day3, part1)]
pub fn part1(input: &Vec<Vec<DirectionAndMagnitude>>) -> i32 {
    let origin = Coordinate::origin();

    let locations1 = visit_locations(input[0].clone());
    let locations2 = visit_locations(input[1].clone());

    let mut min_dist_intersection = i32::MAX;

    for coord in locations1.intersection(&locations2) {
        let dist = origin.manhattan_distance(&coord);
        if dist < min_dist_intersection && dist != 0 {
            min_dist_intersection = dist;
        }
    }

    min_dist_intersection
}

fn visit_locations(directions: Vec<DirectionAndMagnitude>) -> HashSet<Coordinate> {
    let mut locations = HashSet::new();

    let mut location = Coordinate::origin();
    for dir in flatten_directions(directions) {
        location.translate(dir);
        locations.insert(location.clone());
    }

    locations
}

fn visit_locations_with_steps(
    directions: Vec<DirectionAndMagnitude>,
) -> HashMap<Coordinate, usize> {
    let mut locations = HashMap::new();

    let mut location = Coordinate::origin();
    for (idx, dir) in flatten_directions(directions).into_iter().enumerate() {
        location.translate(dir);
        locations.entry(location.clone()).or_insert(idx);
    }

    locations
}

#[aoc(day3, part2)]
pub fn part2(input: &Vec<Vec<DirectionAndMagnitude>>) -> usize {
    let locations1 = visit_locations_with_steps(input[0].clone());
    let locations2 = visit_locations_with_steps(input[1].clone());

    let keys1 = locations1.keys().collect::<HashSet<_>>();
    let keys2 = locations2.keys().collect::<HashSet<_>>();

    let mut min_size_found = usize::max_value();

    for intersection in keys1.intersection(&keys2) {
        let step1 = locations1[intersection];
        let step2 = locations2[intersection];

        let distance = step1 + step2 + 2;

        if distance < min_size_found {
            min_size_found = distance;
        }
    }

    min_size_found
}

#[cfg(test)]
mod tests {
    tests!(3, 1519, 14358);
}
