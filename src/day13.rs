use crate::intcode;
use crate::util;
use aoc_runner_derive::{aoc, aoc_generator};
use image::{Frame, ImageBuffer, Pixel, Rgba};
use std::collections::HashSet;

type Input = Vec<intcode::Value>;

#[aoc_generator(day13)]
pub fn gather_input(input: &str) -> Input {
    util::parse_commas(input)
}

// not 1197417
#[aoc(day13, part1)]
pub fn part1(input: &Input) -> usize {
    let mut program = input.clone();

    let mut state = intcode::State::new(&mut program);

    state.run(vec![]).unwrap();

    let mut blocks = HashSet::new();

    for chunk in state.outputs.chunks(3) {
        if chunk[2] == 2 {
            blocks.insert((chunk[0], chunk[1]));
        } else {
            blocks.remove(&(chunk[0], chunk[1]));
        }
    }

    blocks.len()
}

struct Game {
    score: intcode::Value,
    paddle: (intcode::Value, intcode::Value),
    ball: (intcode::Value, intcode::Value),
    blocks: HashSet<(intcode::Value, intcode::Value)>,
    walls: HashSet<(intcode::Value, intcode::Value)>,
    frame: usize,
    width: u32,
    height: u32,
}

impl Game {
    fn new(width: u32, height: u32) -> Self {
        Self {
            score: 0,
            paddle: (0, 0),
            ball: (0, 0),
            blocks: HashSet::new(),
            walls: HashSet::new(),
            frame: 0,
            width,
            height,
        }
    }

    #[allow(dead_code)]
    fn animate_frame(&self) -> Frame {
        let mut img = ImageBuffer::new(self.width, self.height);

        for (x, y) in &self.walls {
            img.put_pixel(*x as u32, *y as u32, Rgba::from_channels(255, 0, 0, 255));
        }

        for (x, y) in &self.blocks {
            img.put_pixel(*x as u32, *y as u32, Rgba::from_channels(0, 255, 0, 255));
        }

        img.put_pixel(
            self.paddle.0 as u32,
            self.paddle.1 as u32,
            Rgba::from_channels(0, 0, 255, 255),
        );

        img.put_pixel(
            self.ball.0 as u32,
            self.ball.1 as u32,
            Rgba::from_channels(255, 255, 0, 255),
        );

        Frame::new(img)
    }

    fn update(&mut self, outputs: &[intcode::Value]) -> intcode::Value {
        self.frame += 1;

        for chunk in outputs.chunks(3) {
            let position = (chunk[0], chunk[1]);

            if chunk[0] == -1 && chunk[1] == 0 {
                self.score = chunk[2];
                continue;
            }

            match chunk[2] {
                0 => {
                    self.walls.remove(&position);
                    self.blocks.remove(&position);
                }
                1 => {
                    self.walls.insert(position);
                }
                2 => {
                    self.blocks.insert(position);
                }
                3 => {
                    self.paddle = position;
                }
                4 => {
                    self.ball = position;
                }
                _ => continue,
            };
        }

        if self.paddle.0 > self.ball.0 {
            -1
        } else if self.paddle.0 < self.ball.0 {
            1
        } else {
            0
        }
    }
}

// NOT 13568
#[aoc(day13, part2)]
pub fn part2(input: &Input) -> intcode::Value {
    let mut program = input.clone();
    program[0] = 2;

    let mut state = intcode::State::new(&mut program);
    let _ = state.run(vec![]);

    let mut max_x = 0;
    let mut max_y = 0;

    for chunk in state.outputs.chunks(3) {
        if chunk[0] > max_x {
            max_x = chunk[0];
        }

        if chunk[1] > max_y {
            max_y = chunk[1];
        }
    }

    let mut game = Game::new(max_x as u32 + 1, max_y as u32 + 1);
    let mut joystick = game.update(&state.outputs);
    // let mut frames = Vec::new();
    // frames.push(game.animate_frame());

    loop {
        let result = state.run(vec![joystick]);
        joystick = game.update(&state.outputs);
        // frames.push(game.animate_frame());

        state.outputs.clear();

        if game.blocks.is_empty() {
            break;
        }

        if result.is_ok() {
            panic!("Game finished early");
        }
    }

    // let file = std::fs::File::create("day13.gif").unwrap();
    // let mut encoder = image::gif::Encoder::new(file);
    // encoder.encode_frames(frames).unwrap();

    game.score
}

#[cfg(test)]
mod tests {
    tests!(13, 284, 13581);

    test_examples!(
        (
            // "" => 0,
        ),
        (
            // "" => 0,
        )
    );
}
