use crate::util;
use aoc_runner_derive::{aoc, aoc_generator};
use std::collections::HashMap;

type Value = Vec<Vec<String>>;

#[aoc_generator(day6)]
pub fn gather_input(input: &str) -> Value {
    util::parse_lines_and_split(input, ')')
}

#[aoc(day6, part1)]
pub fn part1(program: &Value) -> i32 {
    let mut data = HashMap::new();

    for values in program {
        data.insert(values[1].clone(), values[0].clone());
    }

    let mut count = 0;

    for (mut k, _) in &data {
        while k != "COM" {
            k = &data[k];
            count += 1;
        }
    }

    count
}

#[aoc(day6, part2)]
pub fn part2(program: &Value) -> i32 {
    let mut data = HashMap::new();

    for values in program {
        data.insert(values[1].clone(), values[0].clone());
    }

    let mut chain = Vec::new();
    let mut k = &data["SAN"];

    let mut i = 0;
    while k != "COM" {
        chain.push((i, k.clone()));
        k = &data[k];
        i += 1;
    }

    let mut k = &data["YOU"];
    let mut i = 0;
    while k != "COM" {
        if let Some((n, _)) = chain.iter().filter(|(_, x)| x == k).nth(0) {
            return n + i;
        }

        k = &data[k];
        i += 1;
    }

    panic!("No common ancestor found");
}

#[cfg(test)]
mod tests {
    tests!(6, 301100, 547);
}
