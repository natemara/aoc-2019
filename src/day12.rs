use crate::util;
use aoc_runner_derive::{aoc, aoc_generator};
use itertools::Itertools;
use num::Integer;
use std::collections::HashSet;
use std::ops::{AddAssign, Sub};

#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
pub struct Coord3 {
    x: i64,
    y: i64,
    z: i64,
}

impl AddAssign for Coord3 {
    fn add_assign(&mut self, rhs: Self) {
        self.x += rhs.x;
        self.y += rhs.y;
        self.z += rhs.z;
    }
}

impl Sub for Coord3 {
    type Output = Self;

    fn sub(self, rhs: Self) -> Self {
        Self {
            x: self.x - rhs.x,
            y: self.y - rhs.y,
            z: self.z - rhs.z,
        }
    }
}

impl Coord3 {
    fn energy(&self) -> i64 {
        self.x.abs() + self.y.abs() + self.z.abs()
    }

    fn zero() -> Self {
        Self { x: 0, y: 0, z: 0 }
    }

    fn diff(x: i64, y: i64) -> i64 {
        if x == y {
            0
        } else if x > y {
            1
        } else {
            -1
        }
    }

    fn direction_diff(&self, other: &Self) -> Self {
        let x = Self::diff(self.x, other.x);
        let y = Self::diff(self.y, other.y);
        let z = Self::diff(self.z, other.z);

        Self { x, y, z }
    }
}

type Input = Vec<Coord3>;

#[aoc_generator(day12)]
pub fn gather_input(input: &str) -> Input {
    input
        .lines()
        .map(|line| {
            let nums = util::all_digits(line);

            Coord3 {
                x: nums[0],
                y: nums[1],
                z: nums[2],
            }
        })
        .collect()
}

// not 1197417
#[aoc(day12, part1)]
pub fn part1(input: &Input) -> i64 {
    let mut positions = input.clone();
    let mut velocities = std::iter::repeat(Coord3::zero())
        .take(positions.len())
        .collect::<Vec<_>>();

    for _ in 0..1000 {
        for pair in (0..positions.len()).combinations(2) {
            let pos1 = &positions[pair[0]];
            let pos2 = &positions[pair[1]];

            velocities[pair[0]] += pos2.direction_diff(pos1);
            velocities[pair[1]] += pos1.direction_diff(pos2);
        }

        for i in 0..positions.len() {
            positions[i] += velocities[i];
        }
    }

    energy(&positions, &velocities)
}

fn energy(positions: &[Coord3], velocities: &[Coord3]) -> i64 {
    let mut energy = 0;

    for i in 0..positions.len() {
        energy += positions[i].energy() * velocities[i].energy();
    }

    energy
}

#[aoc(day12, part2)]
pub fn part2(input: &Input) -> i128 {
    let x_positions: Vec<_> = input.iter().map(|p| p.x).collect();
    let y_positions: Vec<_> = input.iter().map(|p| p.y).collect();
    let z_positions: Vec<_> = input.iter().map(|p| p.z).collect();

    let mut cycles = Vec::new();

    for mut positions in vec![x_positions, y_positions, z_positions] {
        let mut steps = 0;
        let mut velocities = std::iter::repeat(0)
            .take(positions.len())
            .collect::<Vec<_>>();

        let mut seen = HashSet::new();
        seen.insert((positions.clone(), velocities.clone()));

        loop {
            steps += 1;

            for pair in (0..positions.len()).combinations(2) {
                let pos1 = positions[pair[0]];
                let pos2 = positions[pair[1]];

                velocities[pair[0]] += Coord3::diff(pos2, pos1);
                velocities[pair[1]] += Coord3::diff(pos1, pos2);
            }

            for i in 0..positions.len() {
                positions[i] += velocities[i];
            }

            if !seen.insert((positions.clone(), velocities.clone())) {
                break;
            }
        }

        cycles.push(steps as i128);
    }

    cycles[0].lcm(&cycles[1]).lcm(&cycles[2])
}

#[cfg(test)]
mod tests {
    tests!(12, 6735, 326489627728984);

    test_examples!(
        (
            // "" => 0,
        ),
        (
            // "" => 0,
        )
    );
}
