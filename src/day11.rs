use crate::directions::*;
use crate::intcode;
use crate::util;
use aoc_runner_derive::{aoc, aoc_generator};
use std::collections::HashMap;

type Value = usize;
type Input = Vec<intcode::Value>;

#[aoc_generator(day11)]
pub fn gather_input(input: &str) -> Input {
    util::parse_commas(input)
}

#[aoc(day11, part1)]
pub fn part1(input: &Input) -> Value {
    let mut painted = HashMap::new();
    let position = Coordinate::origin();

    run(input.clone(), &mut painted, position)
}

fn run(
    mut program: Vec<intcode::Value>,
    painted: &mut HashMap<Coordinate, intcode::Value>,
    mut pos: Coordinate,
) -> usize {
    let mut state = intcode::State::new(&mut program);
    let mut direction = Direction::Up;

    loop {
        let paint_code = painted.get(&pos).unwrap_or(&0);

        if state.run(vec![*paint_code]).is_ok() {
            break;
        }

        painted.insert(pos.clone(), state.outputs[0]);

        match state.outputs[1] {
            0 => direction.ccw(),
            1 => direction.cw(),
            x => panic!("Invalid dir: {}", x),
        }

        pos.translate(direction.clone());

        state.outputs.clear();
    }

    painted.len()
}

fn draw(painted: &HashMap<Coordinate, intcode::Value>) -> String {
    let mut keys = painted.keys();
    let first = keys.next().unwrap();

    let mut min_x = first.x;
    let mut max_x = first.x;
    let mut min_y = first.y;
    let mut max_y = first.y;

    for k in keys {
        if k.x < min_x {
            min_x = k.x;
        } else if k.x > max_x {
            max_x = k.x;
        }

        if k.y < min_y {
            min_y = k.y;
        } else if k.y > max_y {
            max_y = k.y;
        }
    }

    let mut s = String::new();

    s.push('\n');

    for y in min_y..=max_y {
        for x in min_x..=max_x {
            if let Some(1) = painted.get(&Coordinate::new(x, y)) {
                s.push('■');
            } else {
                s.push(' ');
            }
        }
        s.push('\n');
    }

    s
}

// RKURGKGK
#[aoc(day11, part2)]
pub fn part2(input: &Input) -> String {
    let mut painted = HashMap::new();
    let position = Coordinate::origin();

    painted.insert(position.clone(), 1);

    run(input.clone(), &mut painted, position);

    draw(&painted)
}

#[cfg(test)]
mod tests {
    const PART2_ANSWER: &str = concat!(
        "\n",
        " ■■■  ■  ■ ■  ■ ■■■   ■■  ■  ■  ■■  ■  ■   \n",
        " ■  ■ ■ ■  ■  ■ ■  ■ ■  ■ ■ ■  ■  ■ ■ ■    \n",
        " ■  ■ ■■   ■  ■ ■  ■ ■    ■■   ■    ■■     \n",
        " ■■■  ■ ■  ■  ■ ■■■  ■ ■■ ■ ■  ■ ■■ ■ ■    \n",
        " ■ ■  ■ ■  ■  ■ ■ ■  ■  ■ ■ ■  ■  ■ ■ ■    \n",
        " ■  ■ ■  ■  ■■  ■  ■  ■■■ ■  ■  ■■■ ■  ■   \n",
    );

    tests!(11, 1934, PART2_ANSWER);

    test_examples!(
        (
            "3,8,1005,8,315,1106,0,11,0,0,0,104,1,104,0,3,8,1002,8,-1,10,101,1,10,10,4,10,1008,8,0,10,4,10,101,0,8,29,2,1006,16,10,3,8,102,-1,8,10,1001,10,1,10,4,10,1008,8,0,10,4,10,102,1,8,55,3,8,102,-1,8,10,1001,10,1,10,4,10,108,1,8,10,4,10,101,0,8,76,1,101,17,10,1006,0,3,2,1005,2,10,3,8,1002,8,-1,10,1001,10,1,10,4,10,1008,8,1,10,4,10,101,0,8,110,1,107,8,10,3,8,1002,8,-1,10,1001,10,1,10,4,10,108,0,8,10,4,10,101,0,8,135,1,108,19,10,2,7,14,10,2,104,10,10,3,8,1002,8,-1,10,101,1,10,10,4,10,1008,8,1,10,4,10,101,0,8,170,1,1003,12,10,1006,0,98,1006,0,6,1006,0,59,3,8,1002,8,-1,10,1001,10,1,10,4,10,1008,8,0,10,4,10,102,1,8,205,1,4,18,10,1006,0,53,1006,0,47,1006,0,86,3,8,1002,8,-1,10,101,1,10,10,4,10,108,0,8,10,4,10,1001,8,0,239,2,9,12,10,3,8,1002,8,-1,10,1001,10,1,10,4,10,1008,8,1,10,4,10,101,0,8,266,1006,0,8,1,109,12,10,3,8,1002,8,-1,10,1001,10,1,10,4,10,108,1,8,10,4,10,1001,8,0,294,101,1,9,9,1007,9,1035,10,1005,10,15,99,109,637,104,0,104,1,21102,936995730328,1,1,21102,1,332,0,1105,1,436,21102,1,937109070740,1,21101,0,343,0,1106,0,436,3,10,104,0,104,1,3,10,104,0,104,0,3,10,104,0,104,1,3,10,104,0,104,1,3,10,104,0,104,0,3,10,104,0,104,1,21102,1,179410308187,1,21101,0,390,0,1105,1,436,21101,0,29195603035,1,21102,1,401,0,1106,0,436,3,10,104,0,104,0,3,10,104,0,104,0,21102,825016079204,1,1,21102,1,424,0,1105,1,436,21102,1,825544672020,1,21102,435,1,0,1106,0,436,99,109,2,21202,-1,1,1,21102,1,40,2,21102,467,1,3,21101,0,457,0,1105,1,500,109,-2,2106,0,0,0,1,0,0,1,109,2,3,10,204,-1,1001,462,463,478,4,0,1001,462,1,462,108,4,462,10,1006,10,494,1102,0,1,462,109,-2,2106,0,0,0,109,4,1202,-1,1,499,1207,-3,0,10,1006,10,517,21102,1,0,-3,22101,0,-3,1,22101,0,-2,2,21101,1,0,3,21101,0,536,0,1106,0,541,109,-4,2106,0,0,109,5,1207,-3,1,10,1006,10,564,2207,-4,-2,10,1006,10,564,21202,-4,1,-4,1105,1,632,21202,-4,1,1,21201,-3,-1,2,21202,-2,2,3,21101,583,0,0,1106,0,541,22102,1,1,-4,21101,0,1,-1,2207,-4,-2,10,1006,10,602,21101,0,0,-1,22202,-2,-1,-2,2107,0,-3,10,1006,10,624,21202,-1,1,1,21101,624,0,0,106,0,499,21202,-2,-1,-2,22201,-4,-2,-4,109,-5,2106,0,0" => 2339,
        ),
        (
            // "" => 0,
        )
    );
}
